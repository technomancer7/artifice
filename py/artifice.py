import importlib, os

class A_Commands:
    @staticmethod
    def a_echo(artifice, ln):
        if artifice.get_type_of(ln) == "string":
            artifice.send_acc(ln)
        elif artifice.get_type_of(ln) == "number":
            artifice.send_acc(ln)
        elif artifice.get_type_of(ln) == "bool":
            artifice.send_acc(ln)
        elif artifice.get_type_of(ln) == "name":
            if ln.startswith("@"):
                artifice.send_acc(artifice.file_contents(ln.replace("@", "")))
            else:
                artifice.send_acc(artifice.getvars(ln))
        else:
            print("Can't interpret input...")

    @staticmethod
    def a_help(artifice, line):
        if line == "":
            for c in artifice.commands:
                for con in artifice.commands[c]:
                    print(f"{c}.{con}")
        else:
            if line.startswith("lib "):
                if(artifice.libMeta.get(line.split(" ")[1])):
                    m = artifice.libMeta[line.split(" ")[1]]
                    if m.get("name"):
                        print(m["name"])
                    if m.get("description"):
                        print(m["description"])
                    if m.get("author"):
                        print("By ",m['author'])
                    if m.get("homepage"):
                        print("@ ",m['homepage'])    
                else:
                    print(line, "not loaded.")
            else:
                packagename = "*"
                commandname = line
                if "." in line:
                    packagename = line.split(".")[0]
                    commandname = line.split(".")[1]

                if artifice.commands.get(packagename) and artifice.commands[packagename].get(commandname):
                    if artifice.commands[packagename][commandname].get("usage"):
                        print(packagename+"."+artifice.commands[packagename][commandname]["usage"])
                    else:
                        print(packagename+"."+line)

                    if artifice.commands[packagename][commandname].get("help"):
                        print(artifice.commands[packagename][commandname]["help"])
                    
                    if artifice.commands[packagename][commandname].get("scope"):
                        s = ", ".join(artifice.commands[packagename][commandname]["scope"])
                        print("In scope:", s)
                    else:
                        print("In scope: *")

    @staticmethod
    def a_append(artifice, line):
        name = line.split(" ")[0]
        v = " ".join(line.split(" ")[1:])
        print(v)
        if artifice.variables.get(name):
            artifice.append_to_var(name, v)

    @staticmethod
    def a_prepend(artifice, line):
        name = line.split(" ")[0]
        v = " ".join(line.split(" ")[1:])
        if artifice.variables.get(name):
            artifice.prepend_to_var(name, v)

    @staticmethod
    def a_defn(artifice, line):
        vtype = line.split(" ")[0]
        name = line.split(" ")[1]
        if not artifice.variables.get(name):
            artifice.new_variable(name, artifice.default_value_of(vtype), vtype)
        else:
            artifice.send_acc("Variable already defined "+name)

    @staticmethod
    def a_setv(artifice, line):
        name = line.split("=")[0].strip()
        val = "=".join(line.split("=")[1:]).strip()
        if name.startswith("_"):
            artifice.send_acc("System variables can not be set this way.")
            return 

        if artifice.getvarpt(name) == None:
            print("Variable "+name+" not defined.")
        else:
            if artifice.get_type_of(val) != artifice.getvar(name)["type"]:
                print("Type mismatch")
            else:
                artifice.setvars(name, val)

    @staticmethod
    def a_getv(artifice, name):            
        if artifice.getvarpt(name) == None:
            print("Variable "+name+" not defined.")
        else:
            return artifice.getvars(name)

    @staticmethod
    def a_exp(artifice, name):    
        pass

    @staticmethod
    def a_if(artifice, name):    
        pass

    @staticmethod
    def a_label(artifice, name):    
        pass

    @staticmethod
    def a_jump(artifice, name):    
        pass

class ArtificeVariables:
    def vcontents(self, line):
        if self.get_type_of(line) == "string":
            return line[1:-1]
        if self.get_type_of(line) in ["bool", "number"]:
            return line
        if self.get_type_of(line) == "name":
            if line.startswith("@"):
                return self.file_contents(line[1:])
            else:
                return self.getvars(line)

    def getvar(self, name):
        if self.variables.get(name):
            return self.variables[name]

    def getvars(self, name):
        if self.variables.get(name):
            return self.variables[name]["value"]

    def getvarpt(self, name):
        if self.variables.get(name):
            t = self.variables[name]["type"]
            v = self.variables[name]["value"]
            if t == "string":
                return '"'+v+'"'
            elif t == "name":
                return v
            elif t == "number":
                return int(v)
            elif t == "bool" and v == "true":
                return True
            elif t == "bool" and v == "false":
                return False
        else:
            return None
              
    def setvars(self, name, value):
        try:
            int(value)
            self.new_variable(name, value, 'number')
        except:
            pass

        if value.lower() == "true": 
            self.new_variable(name, value.upper(), 'bool')
        elif value.lower() == "false": 
            self.new_variable(name, value.upper(), 'bool')
        elif self.get_type_of(value) == "string":
            self.new_variable(name, value, 'string')
        elif self.get_type_of(value) == "name":
            self.new_variable(name, value, 'name')

    def setvarpt(self, name, value):
        if type(value) == int: self.new_variable(name, str(value), 'number')
        elif value == True: self.new_variable(name, "TRUE", 'bool')
        elif value == False: self.new_variable(name, "FALSE", 'bool')
        elif type(value) == str: 
            if self.get_type_of(value) == "string":
                self.new_variable(name, value, 'string')
            if self.get_type_of(value) == "name":
                self.new_variable(name, value, 'name')
        
    def append_to_var(self, name, ln):
        if self.get_type_of(name) == "name" and self.get_type_of(ln) == "string":

            self.setvars(name, '"'+self.getvars(name)[1:-1]+ln.replace("<br>", "\n")[1:-1]+'"')

        if self.get_type_of(name) == "name" and self.get_type_of(ln) == "name" and self.getvar(ln)["type"] == "string":
            self.setvars(name, '"'+self.getvars(name)[1:-1]+self.getvars(ln).replace("<br>", "\n")[1:-1]+'"')

    def prepend_to_var(self, name, ln):
        if self.get_type_of(name) == "name" and self.get_type_of(ln) == "string":
            self.setvars(name, '"'+ln.replace("<br>", "\n")[1:-1]+self.getvars(name)[1:-1]+'"')

        if self.get_type_of(name) == "name" and self.get_type_of(ln) == "name" and self.getvar(ln)["type"] == "string":
            self.setvars(name, '"'+self.getvars(ln).replace("<br>", "\n")[1:-1]+self.getvars(name)[1:-1]+'"')

    def new_variable(self, name, data, dtype):
        self.variables[name] = {"value": data, "type": dtype, "name": name}

    def default_value_of(self, vtype):
        if vtype == "string":
            return ""
        elif vtype == "number":
            return "0"
        elif vtype == "bool":
            return "FALSE"
        else:
            return ""

    def get_type_of(self, value):
        if value.startswith("'") and value.endswith("'"):
            return "string"
        if value.startswith('"') and value.endswith('"'):
            return "string"  
        if value.startswith("`") and value.endswith("`"):
            return "string" 
        if value == "FALSE" or value == "TRUE":
            return "bool"
        try:
            int(value)
            return "number"
        except:
            pass


        return "name"

class ArtificeAccumulator:
    def flush_acc(self):
        self.print_acc()
        self.clear_acc()

    def print_acc(self):
        for ln in self.acc:
            print(ln)

    def send_acc(self, line):
        ln = self.getvarpt('_LINE')
        self.acc.append(f"{ln+1}: {line}")

    def clear_acc(self):
        self.acc.clear()

class ArtificeSignals:
    def add_signal(self, group, name, fn):
        if not self.signals.get(group):
            self.signals[group] = {}
        self.signals[group][name] = fn

    def execute_signal(self, group, name, line):
        if self.signals.get(group) and self.signals[group].get(name):
            self.signals[group][name](self, line)
            
    def execute_signals(self, group, line):
        if self.signals.get(group):
            for sig in self.signals[group].keys():
                self.signals[group][sig](self, line)
          
class ArtificeFile:
    def make_file(self, path):
        if os.path.exists(path):
            return

        with open(path, 'w+') as f:
            f.write("")

    def delete_file(self, path):
        pass
    
    def delete_file_from_handle(self, name):
        pass

    def move_file(self, path, newpath):
        pass

    def move_file_from_handle(self, name, newpath):
        pass  

    def grab_file(self, name, path):
        if self.fileHandles.get(name):
            print("File handle "+name+" already taken.")
        
        if not os.path.exists(path):
            print("File not found "+path)
        with open(path, 'r') as f:   
            self.fileHandles[name] = {"name": name, "path": path, "body": f.read()}

    def drop_file(self, name):
        if self.fileHandles.get(name):
            del self.fileHandles[name]  

    def write_to_file(self, name, text):
        if self.fileHandles.get(name):
            self.fileHandles[name]["body"] = text

    def append_to_file(self, name, text, mid = "\n"):
        if self.fileHandles.get(name):
            self.fileHandles[name]["body"] = self.fileHandles[name]["body"]+mid+text


    def prepend_to_file(self, name, text, mid = "\n"):
        if self.fileHandles.get(name):
            self.fileHandles[name]["body"] = text+mid+self.fileHandles[name]["body"]

    def file_contents(self, name):
        print(name)
        print(self.fileHandles)
        if self.fileHandles.get(name):
            return self.fileHandles[name]["body"]

    def replace_in_file(self, name, oldt, newt):
        #TODO make a version for regex
        if self.fileHandles.get(name):
            self.fileHandles[name]["body"] = self.fileHandles[name]["body"].replace(oldt, newt)


    def save_file(self, name):
        if self.fileHandles.get(name):
            with open(self.fileHandles[name]["path"], 'w+') as f:
                f.write(self.fileHandles[name]["body"])

    def reload_file(self, name):
        if self.fileHandles.get(name):
            with open(self.fileHandles[name]["path"], 'r') as f:   
                self.fileHandles[name]["body"] = f.read()

class Artifice(ArtificeAccumulator, ArtificeSignals, ArtificeVariables, ArtificeFile):
    def __init__(self):
        self.signals = {}
        self.fileHandles = {}
        self.libMeta = {}
        self.commands = {
            "*": { 
                "write": {
                    "execute": lambda art, ln: print(ln),
                    "scope": ["runtime"]
                },
                "halt": {
                    "execute": lambda a, ln: quit(),
                    "scope": ["*"]
                },
                "echo": {
                    "execute": A_Commands.a_echo,
                    "scope": ["runtime", "repl"]
                },
                "link": {
                    "help": "",
                    "usage": "",
                    "scope": ["compile", "repl"],
                    "execute": lambda art, ln : art.linkLibrary(ln)
                },
                "define": {
                    "help": "",
                    "usage": "",
                    "scope": ["compile"],
                    "execute": A_Commands.a_defn
                },
                "set": {
                    "help": "",
                    "usage": "",
                    "scope": ["runtime", "repl"],
                    "execute": A_Commands.a_setv
                },
                "append": {
                    "help": "",
                    "usage": "",
                    "scope": ["runtime", "repl"],
                    "execute": A_Commands.a_append
                },
                "prepend": {
                    "help": "",
                    "usage": "",
                    "scope": ["runtime", "repl"],
                    "execute": A_Commands.a_prepend
                },
                "if": {
                    "help": "",
                    "usage": "",
                    "scope": ["runtime", "repl"],
                    "execute": A_Commands.a_if
                },
                "label": {
                    "help": "",
                    "usage": "",
                    "scope": ["compile"],
                    "execute": A_Commands.a_label
                },
                "jump": {
                    "help": "",
                    "usage": "",
                    "scope": ["runtime"],
                    "execute": A_Commands.a_jump
                },
                "exp": {
                    "help": "Runs command using standard parsing, and if that command returns an out, it is stored in value var.\nValue can be a variable name or file handle",
                    "usage": "exp var << command",
                    "scope": ["runtime", "repl"],
                    "execute": A_Commands.a_exp
                },
                "help": {
                    "help": "Help system!",
                    "execute": A_Commands.a_help,
                    "scope": ["repl"]
                },
                "acc": {
                    "help": "Flushes the accumulator, echoing back its entire contents and then clearing.",
                    "execute": lambda art, ln: art.flush_acc()
                },
                "note": {
                    "execute": lambda art, ln: ln
                }
            }
        }
        self.variables = {}
        self.lines = []
        self.body = ""
        self.acc = []

    def linkLibrary(self, libname):
        package = libname
        if "=" in libname:
            package = libname.split("=")[0].strip()
            libname = libname.split("=")[1].strip()

        module = importlib.import_module("lib."+libname)
        module.loadLibrary(self, package)
        self.libMeta[package] = module.meta()
    def parse(self, line):
        #print(self.commands.keys())
        package = "*"
        cmd = line.split(" ")[0]
        ln = " ".join(line.split(" ")[1:])

        if "." in cmd:
            package = cmd.split(".")[0]
            cmd = cmd.split(".")[1]
        #print("Trying "+cmd, package)
        if self.commands.get(package):
            #print("Package check")
            if self.commands[package].get(cmd):
                #print("Command check")
                if not self.commands[package][cmd].get("scope"):
                    #print("Executing anyscope "+cmd)
                    self.commands[package][cmd]["execute"](self, ln)
                else:
                    #print(self.commands[package][cmd]["scope"])
                    #print(self.getvarpt("_MODE"))
                    if self.getvarpt("_MODE") in self.commands[package][cmd]["scope"] or "*" in self.commands[package][cmd]["scope"]:
                        #print("Executing scoped "+cmd)
                        self.commands[package][cmd]["execute"](self, ln)
            else:
                self.send_acc("cmd not found "+cmd)
        else:
            self.send_acc("package not found "+package)

    def compile(self):
        self.setvarpt("_MODE", "compile")
        self.setvarpt("_LINE", -1)
        for line in self.lines:
            self.setvarpt("_LINE", self.getvarpt("_LINE") + 1)
            if line.strip() != "":
                self.parse(line)

    def loadFile(self, path):
        with open(path, 'r') as f:
            self.body = f.read()
            self.lines = self.body.split("\n")
            #while "" in self.lines:
                #self.lines.remove("")

    def run(self):
        self.setvarpt("_MODE", "runtime")
        self.setvarpt("_LINE", 0)

        while self.getvarpt("_LINE") < len(self.lines):
            if self.lines[self.getvarpt("_LINE")].strip() != "":
                self.parse(self.lines[self.getvarpt("_LINE")])
            self.setvarpt("_LINE", self.getvarpt("_LINE") + 1)