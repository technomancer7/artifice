
def meta():
    return {
        "name": "file",
        "description": "Commands for filesystem access and management.",
        "author": "Kaiser"
    }
        

def loadLibrary(artifice, namespace = None):
    if namespace == None:
        namespace = "file"
    
    if not artifice.commands.get(namespace):
        artifice.commands[namespace] = {}

    artifice.commands[namespace]["make"] = {
        "execute": lambda art, ln : art.make_file(ln),
        "scope": ["runtime", "repl"],
        "usage": "make filepath",
        "help": "Creates new file on the drive at filepath."
    }

    def getfile(art, ln):
        name = ln.split("=")[0].strip()
        path = "=".join(ln.split("=")[1:]).strip()
        art.grab_file(name, path)

    artifice.commands[namespace]["grab"] = {
        "execute": getfile,
        "scope": ["runtime", "repl"],
        "usage": "grab handlename = path",
        "help": "Opens a new file handle from drive file at path."
    }

    def writefile(art, ln):
        name = ln.split("=")[0].strip()
        line = "=".join(ln.split("=")[1:]).strip()

        art.write_to_file(name, art.vcontents(line))

        

    artifice.commands[namespace]["write"] = {
        "execute": writefile,
        "scope": ["runtime", "repl"]
    }

    def readfile(art, ln):
        name = ln.split("=")[0].strip()
        filehandler = ln.split("=")[1].strip()[1:]
        if art.variables.get(name):
            print("File", filehandler)
            text = art.file_contents(filehandler)
            art.setvars(name, '"'+text+'"')


    artifice.commands[namespace]["read"] = {
        "execute": readfile,
        "scope": ["runtime", "repl"]
    }
      
    def modfile(art, ln):
        if ">|" in ln:
            name = ln.split(">|")[0].strip()
            line = ">|".join(ln.split(">|")[1:]).strip()

            art.append_to_file(name, art.vcontents(line))

        elif "|<" in ln:
            name = ln.split("|<")[0].strip()
            line = "|<".join(ln.split("|<")[1:]).strip()
            art.prepend_to_file(name, art.vcontents(line))   

    artifice.commands[namespace]["mod"] = {
        "execute": modfile,
        "scope": ["runtime", "repl"]
    }
    
    artifice.commands[namespace]["drop"] = {
        "execute": lambda art, ln : art.drop_file(ln),
        "scope": ["runtime", "repl"]
    }

    artifice.commands[namespace]["save"] = {
        "execute": lambda art, ln : art.save_file(ln),
        "scope": ["runtime", "repl"]
    }
