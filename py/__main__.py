import artifice, sys
ar = artifice.Artifice()

if len(sys.argv) == 1:
    ar.setvarpt("_MODE", "repl")
    ar.setvarpt("_LINE", -1)
    while True:
        ln = input("> ")
        ar.parse(ln)
        ar.print_acc()
        ar.clear_acc()
else:
    ar.loadFile(sys.argv[1])
    ar.compile()
    ar.run()