require "libtcm"

Artifice = {
    commands = {
        ["*"] = {
            link = {}
        }
    },
    variables = {}
}

Artifice_Var = {
    name = "",
    type = "",
    value = ""
}

function Artifice.newVariable(self, name, dtype, data)
    local v = Artifice_Var.new()
    v.name = name
    v.type = dtype
    v.value = data
    self.variables[name] = v
end

function Artifice.newCommand(self, namespace, name, fn)
    if self.commands[namespace] == nil then self.commands[namespace] = {} end
    self.commands[namespace][name] = {execute = fn}
end

function Artifice.modifyCommand(self, namespace, name, key, value)
    if self.commands[namespace] == nil then return end
    if self.commands[namespace][name] == nil then return end
    self.commands[namespace][name][key] = value
end



function Artifice.new()
    return Implement(Artifice)
end


function Artifice_Var.new()
    return Implement(Artifice_Var)
end